const transporter = require('../config/config');

// la fonction pour envoyer des e-mails
function CommandEmail(object) {

    const { email, IdCommand } = object;
    let viewCommandUrl = process.env.BASE_URL + `commands/${IdCommand}`;

    let mailOptions = {
        from: process.env.EMAIL_USER,
        to: email,
        subject: 'Détails de votre commande',
        html: `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
         <head>
          <meta charset="UTF-8">
          <meta content="width=device-width, initial-scale=1" name="viewport">
          <meta name="x-apple-disable-message-reformatting">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta content="telephone=no" name="format-detection">
          <title>New message</title><!--[if (mso 16)]>
            <style type="text/css">
            a {text-decoration: none;}
            </style>
            <![endif]--><!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--><!--[if gte mso 9]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG></o:AllowPNG>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
          <style type="text/css">
        .rollover:hover .rollover-first {
          max-height:0px!important;
          display:none!important;
          }
          .rollover:hover .rollover-second {
          max-height:none!important;
          display:inline-block!important;
          }
          .rollover div {
          font-size:0px;
          }
          u ~ div img + div > div {
          display:none;
          }
          #outlook a {
          padding:0;
          }
          span.MsoHyperlink,
        span.MsoHyperlinkFollowed {
          color:inherit;
          mso-style-priority:99;
          }
          a.es-button {
          mso-style-priority:100!important;
          text-decoration:none!important;
          }
          a[x-apple-data-detectors] {
          color:inherit!important;
          text-decoration:none!important;
          font-size:inherit!important;
          font-family:inherit!important;
          font-weight:inherit!important;
          line-height:inherit!important;
          }
          .es-desk-hidden {
          display:none;
          float:left;
          overflow:hidden;
          width:0;
          max-height:0;
          line-height:0;
          mso-hide:all;
          }
          .es-header-body a:hover {
          color:#2cb543!important;
          }
          .es-content-body a:hover {
          color:#2cb543!important;
          }
          .es-footer-body a:hover {
          color:#ffffff!important;
          }
          .es-infoblock a:hover {
          color:#cccccc!important;
          }
          .es-button-border:hover {
          border-color:#42d159 #42d159 #42d159 #42d159!important;
          background:#56d66b!important;
          }
          .es-button-border:hover a.es-button,
        .es-button-border:hover button.es-button {
          background:#56d66b!important;
          }
          td .es-button-border:hover a.es-button-8288 {
          border-color:#333333!important;
          background:#333333!important;
          color:#ffffff!important;
          }
          td .es-button-border-2792:hover {
          border-color:undefined!important;
          background:#333333!important;
          }
        @media only screen and (max-width:600px) {*[class="gmail-fix"] { display:none!important } p, a { line-height:150%!important } h1, h1 a { line-height:120%!important } h2, h2 a { line-height:120%!important } h3, h3 a { line-height:120%!important } h4, h4 a { line-height:120%!important } h5, h5 a { line-height:120%!important } h6, h6 a { line-height:120%!important } .es-header-body p { } .es-content-body p { } .es-footer-body p { } .es-infoblock p { } h1 { font-size:30px!important; text-align:left } h2 { font-size:24px!important; text-align:left } h3 { font-size:20px!important; text-align:left } h4 { font-size:24px!important; text-align:left } h5 { font-size:20px!important; text-align:left } h6 { font-size:16px!important; text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:24px!important } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important } .es-header-body h4 a, .es-content-body h4 a, .es-footer-body h4 a { font-size:24px!important } .es-header-body h5 a, .es-content-body h5 a, .es-footer-body h5 a { font-size:20px!important } .es-header-body h6 a, .es-content-body h6 a, .es-footer-body h6 a { font-size:16px!important } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock a { font-size:12px!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3, .es-m-txt-c h4, .es-m-txt-c h5, .es-m-txt-c h6 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3, .es-m-txt-r h4, .es-m-txt-r h5, .es-m-txt-r h6 { text-align:right!important } .es-m-txt-j, .es-m-txt-j h1, .es-m-txt-j h2, .es-m-txt-j h3, .es-m-txt-j h4, .es-m-txt-j h5, .es-m-txt-j h6 { text-align:justify!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3, .es-m-txt-l h4, .es-m-txt-l h5, .es-m-txt-l h6 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img, .es-m-txt-r .rollover:hover .rollover-second, .es-m-txt-c .rollover:hover .rollover-second, .es-m-txt-l .rollover:hover .rollover-second { display:inline!important } .es-m-txt-r .rollover div, .es-m-txt-c .rollover div, .es-m-txt-l .rollover div { line-height:0!important; font-size:0!important } .es-spacer { display:inline-table } a.es-button, button.es-button { font-size:18px!important } .es-m-fw, .es-m-fw.es-fw, .es-m-fw .es-button { display:block!important } .es-m-il, .es-m-il .es-button, .es-social, .es-social td, .es-menu { display:inline-block!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .adapt-img { width:100%!important; height:auto!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } .es-social td { padding-bottom:10px } .h-auto { height:auto!important } a.es-button, button.es-button { display:inline-block!important } .es-button-border { display:inline-block!important } }
        </style>
         </head>
         <body><div class="es-wrapper-color"><!--[if gte mso 9]>
         <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
             <v:fill type="tile" color="#f6f6f6"></v:fill>
         </v:background>
     <![endif]--><table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td class="esd-email-paddings" valign="top"><table class="es-content" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="esd-stripe" align="center"><table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center"><tbody><tr>
         <td class="esd-structure" align="left">
           <table cellpadding="0" cellspacing="0">
           <tbody><tr>
               <td width="600" class="esd-container-frame" align="left">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation">
                       <tbody><tr>
         <td align="center" class="esd-block-spacer" style="font-size: 0" height="40" bgcolor="#f6f6f6">
         </td>
     </tr></tbody></table>
               </td>
           </tr>
         </tbody></table>
         </td>
       </tr><tr><td class="es-p20t es-p20r es-p20l esd-structure" align="left"><table width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td class="esd-container-frame" width="560" valign="top" align="center"><table width="100%" cellspacing="0" cellpadding="0"><tbody><tr>
         <td align="left" class="esd-block-text">
             <h1 style="font-family:-apple-system,BlinkMacSystemFont,&#39;Segoe UI&#39;,Roboto,Helvetica,Arial,sans-serif,&#39;Apple Color Emoji&#39;,&#39;Segoe UI Emoji&#39;,&#39;Segoe UI Symbol&#39;">Commande</h1>            </td>
     </tr></tbody></table></td></tr></tbody></table></td></tr><tr>
         <td class="esd-structure es-p20t es-p20r es-p20l" align="left">
           <table cellpadding="0" cellspacing="0">
           <tbody><tr>
               <td width="560" class="esd-container-frame" align="left">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation">
                       <tbody><tr>
         <td align="left" class="esd-block-text">
             <p>Cher(e) client(e) de GoodFood,</p><p> </p><p>Merci d'avoir choisi GoodFood pour votre commande culinaire. Nous vous envoyons cet e-mail pour vous fournir une validation que votre commande à bien était prise en compte. Numéro de commande : ${IdCommand}</p>
             <p> </p>
         </td>
     </tr><tr>
         <td align="center" class="esd-block-spacer es-p20" style="font-size: 0">
             <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" class="es-spacer">
                 <tbody>
                     <tr>
                         <td style="border-bottom: 1px solid #cccccc;; background: none; height: 1px; width: 100%; margin: 0px 0px 0px 0px"></td>
                     </tr>
                 </tbody>
             </table>
         </td>
     </tr><tr>
         <td align="left" class="esd-block-text">
             <p>Nous souhaitons vous rappeler que la satisfaction de nos clients est notre priorité. Si vous avez des questions, des modifications à apporter à votre commande ou si vous avez besoin d'une assistance supplémentaire, n'hésitez pas à nous contacter.</p><p> </p><p> Notre équipe du service client est disponible pour vous aider.</p><p>Nous tenons à vous remercier de votre confiance et de votre soutien continu. Nous espérons que cette commande vous apportera entière satisfaction et que vous apprécierez chaque bouchée de nos délicieux plats.</p><p> </p><p>Nous sommes impatients de vous servir à nouveau dans un avenir proche. Bon appétit !</p><p> </p><p>Cordialement,
L'équipe GoodFood</p>
         </td>
     </tr><tr>
         <td align="center" class="esd-block-spacer" style="font-size: 0" height="40">
             
         </td>
     </tr></tbody></table>
               </td>
           </tr>
         </tbody></table>
         </td>
       </tr><tr>
         <td class="esd-structure" align="left">
           <table cellpadding="0" cellspacing="0">
           <tbody><tr>
               <td width="600" class="esd-container-frame" align="left">
                   <table cellpadding="0" cellspacing="0" width="100%" role="presentation">
                       <tbody><tr>
         <td align="center" class="esd-block-spacer" style="font-size: 0" bgcolor="#f6f6f6" height="40">
         </td>
     </tr></tbody></table>
               </td>
           </tr>
         </tbody></table>
         </td>
       </tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div></body>
        `
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

module.exports = CommandEmail;