// charger les bibliothèques
const amqp = require('amqplib');
const SendCommandEmail = require("./functions/CommandEmail");
const SendForgotPasswordEmail = require("./functions/ForgotPasswordEmail");
const SendWelcomeEmail = require("./functions/WelcomeEmail");
const SendDeliveryFinishedEmail = require("./functions/DeliveryFinishedEmail");

let commandMap = {
    "sendCommandEmail": SendCommandEmail,
    "sendForgotPasswordEmail": SendForgotPasswordEmail,
    "sendWelcomeEmail": SendWelcomeEmail,
    "sendDeliveryFinishedEmail": SendDeliveryFinishedEmail
};

// connexion à RabbitMQ
async function consume() {
    try {
        let connection = await amqp.connect('amqp://'+ process.env.RABBITMQ_HOSTNAME) // connexion au serveur local RabbitMQ
        let channel = await connection.createChannel(); // créer un canal

        channel.consume("mailing", (msg) => {
            let message = JSON.parse(msg.content.toString()); // supposer que le message est du JSON
            console.log("Message reçu: ", message);

            if (message.command in commandMap) {
                
                // ici, nous appelons la fonction appropriée en fonction de la commande reçue
                commandMap[message.command](message.myObject);
            } else {
                console.log("Commande non reconnue: " + message.command);
            }

            channel.ack(msg);
        });

        console.log("En attente de messages...");
    } catch (ex) {
        console.error(ex);
    }
}

consume();
