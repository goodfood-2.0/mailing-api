const nodemailer = require("nodemailer");
require('dotenv').config();

// créer le transporter nodemailer pour envoyer les e-mails
let transporter = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE, // utiliser le service Gmail, ou autre service si vous le souhaitez
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS
    }
});

module.exports = transporter;
